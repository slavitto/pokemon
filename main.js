// Создать класс Pokemon, конструктор которого принимает имя и уровень в качестве аргумента. 
// Все экземпляры этого класса должны иметь общий метод show, который выводит информацию о покемоне.
// Создать класс pokemonlist, который в качестве аргументов принимает любое количество покемонов. 
// Экземпляры этого класса должны обладать всеми функциями массива. 
// А так же иметь метод add, который принимает в качестве аргументов имя и уровень, 
// создает нового покемона и добавляет его в список.
// Создать два списка покемонов и сохранить их в переменных lost и found. 
// Имена и уровни придумайте самостоятельно.
// Добавить несколько новых покемонов в каждый список.
// Добавить спискам покемонов метод show, который выводит информацию о покемонах и их общее количество в списке.
// Перевести одного из покемонов из списка lost в список found

"use strict";
const pokemons = require('./pokemons');
const Pokemon  = require('./classes/pokemon');
const PokemonList  = require('./classes/pokemonList');

// создаем покемонов
let lostPokemon = new Pokemon('Charmeleon', 1 );
// for(var i = 0, pokemon = [], poke; i <= 3; i++) {
// 	poke = new Pokemon(pokemons[i].name, pokemons[i].level);
// 	pokemon.push(poke);
// }

// создаем покемонов методом map
let pokemon = [];
pokemons.map(poke => {
	poke = new Pokemon(poke.name, poke.level);
	pokemon.push(poke); 
});

// создаем списки lost и found
let lost = new PokemonList(pokemon[0], lostPokemon, pokemon[3]);
let found = new PokemonList(pokemon[1], pokemon[2]);

// добавляем в отдельные списки
lost.add('Vasilion', 18);
found.add('Petyanon', 18);

// переводим покемона из lost в found
lost.move('Vasilion', found);

// выводим списки и покемона с максимальным уровнем
lost.show();
found.show();
lost.max().show();