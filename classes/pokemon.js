"use strict";
module.exports = class Pokemon {

	constructor(name, level) {
		this.name = name;
		this.level = level;
	}

	show() {
		console.log( `Имя покемона: ${this.name}, Уровень: ${this.level}` ); 
	}

	valueOf() {
        return this.level;
    }

}