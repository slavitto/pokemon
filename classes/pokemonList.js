"use strict";
const Pokemon  = require('./pokemon');
module.exports = class PokemonList extends Array {
	add(name, level) {
		var pokemon = new Pokemon(name, level);
		this.push(pokemon);
	}

	show() {
		for (let pokemon of this) {
            pokemon.show();
        }
		console.log(`Общее число покемонов: ${this.length} \n`);
	}

	move(name, target) {
		var pokemonIndex = this.findIndex(pokemon => pokemon.name === name);
		target.push(this[pokemonIndex]);
		this.splice(pokemonIndex, 1);
	}

	max() {
		this.sort((a,b) => b - a);
        return this[0];
	}

}